<?php

// ini_set('display_startup_errors', 1);
// ini_set('display_errors', 1);
// error_reporting(-1);
// error_reporting(E_ERROR | E_PARSE);

ini_set('max_execution_time', 300);
set_time_limit(300); // gives true

// HTML5 PHP parse library:
// https://github.com/Masterminds/html5-php
// https://stackoverflow.com/questions/10712503/how-to-make-html5-work-with-domdocument

$CSS_PATH_POST = 'wp-content-extractor-post.css';
$CSS_PATH_CATEGORY = 'wp-content-extractor-category.css';

// $NEW_DOMAIN = 'wydzialy.umk.pl/wfaiis/kis';
$DEST_DOMAIN = 'www.fizyka.umk.pl/kis';
$DEST_DOMAIN_EN = 'www.fizyka.umk.pl/en/doi';
$SOURCE_DOMAIN = 'www.is.umk.pl';
$SOURCE_DOMAIN_EN = 'www.is.umk.pl/en';

$CACHE_DIR = '/var/tmp';

// try {

/*
function get_inner_html( $node ) {
    $innerHTML= '';
    $children = $node->childNodes;
    foreach ($children as $child)
        $innerHTML .= $child->ownerDocument->saveXML($child);

    return $innerHTML;
}
*/

function prepareInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$CACHE_FILE_PREFIX = $CACHE_DIR . DIRECTORY_SEPARATOR . basename(__FILE__, '.php') . '.';

$clearCache = prepareInput(!empty($_REQUEST['clearcache']) ? $_REQUEST['clearcache'] : '');  // Header Last Modified plugin doesn't refresh "Last Modiefied" header sometimes...
if ($clearCache === 'true') {
    foreach (glob("$CACHE_FILE_PREFIX*") as $fileName) {
        echo (unlink($fileName) ? 'Deleted' : 'Failed to delete') . ": $fileName<br>";
    }
    exit(0);
}

$url = prepareInput(!empty($_REQUEST['url']) ? $_REQUEST['url'] : '');
if (empty($url)) die('Error: Empty URL');

$headers = get_headers($url, 1);
$lastModifiedTime = strtotime(!empty($headers['Last-Modified']) ? $headers['Last-Modified'] : '');
$CACHE_FILE_PATH = $CACHE_FILE_PREFIX . base64_encode($url);

if (($lastModifiedTime !== false) && is_file($CACHE_FILE_PATH)) {
    // unlink($CACHE_FILE_PATH);
    $cacheModifiedTime = filemtime($CACHE_FILE_PATH);
    // echo date('D, d M Y H:i:s', $lastModifiedTime) . '<=' . date('D, d M Y H:i:s', $cacheModifiedTime);
    if ($lastModifiedTime <= $cacheModifiedTime) {
        echo file_get_contents($CACHE_FILE_PATH);  // output cached file
        exit(0);
    }
}

$parsedurl = parse_url($url);
$html = file_get_contents($url);

$doc = new DOMDocument();
$out = new DOMDocument();
$xpath = new DomXPath($out);

$doc->loadHTML('<?xml encoding="UTF-8">' . $html);
// $html->loadHTML($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

$css = file_get_contents($CSS_PATH_POST);
$style = $out->createElement('style', $css);
$out->appendChild($style);

// Add primary content from $url:
$out->appendChild($out->importNode($doc->getElementById('primary'), true));

// if url is a link to category site -> include content of all posts
// if (strpos($parsedurl['path'], '/category') === 0)
if (preg_match('/^(\/en)?\/category/', $parsedurl['path'])) {
    $css = file_get_contents($CSS_PATH_CATEGORY);
    $style = $out->createElement('style', $css);
    $out->appendChild($style);

    $articlecontents = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' article-content ')]");
    foreach ($articlecontents as $ac) {
        $parent = $ac->parentNode;
        // $morelink = $xpath->query(".//a[contains(concat(' ', normalize-space(@class), ' '), ' more-link ')]", $ac);
        $morelink = $xpath->query(".//header/*/a", $ac);
        $posturl = $morelink->item(0)->getAttribute('href');
        $posthtml = file_get_contents($posturl);
        $postdoc = new DOMDocument();
        $postdoc->loadHTML('<?xml encoding="UTF-8">' . $posthtml);
        $parent->replaceChild($out->importNode($postdoc->getElementsByTagName('article')->item(0)->firstChild, true), $ac); // first child of article is div.article-content
        // $parent->appendChild($out->createElement('hr'));
        // $posthtml = null;
    }

    $previousLinks = $xpath->query("//li[contains(concat(' ', normalize-space(@class), ' '), ' previous ')]/a[contains(@href, '/page/')]");
    if ($previousLinks->length > 0) {  // if more category posts available under previous link => show "more link" pointing to original domain
        $categoryLinks = $xpath->query("//a[contains(@href, '/category/')]");
        $dotsLinkToCategory = $out->createElement('a', '&ctdot;');
        $dotsLinkToCategory->setAttribute('href', $categoryLinks->item(0)->getAttribute('href'));
        $dotsLinkToCategory->setAttribute('class', 'more-link');
        $parent->parentNode->parentNode->appendChild($dotsLinkToCategory);
    }
}

// Remove Simple Share Buttons:
$ssba = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' ssba-wrap ')]"); // Simple Share Buttons Adder (7.3.10) simplesharebuttons.com --><div class="ssba ssba-wrap">
foreach ($ssba as $ss)
    $ss->parentNode->removeChild($ss);

// Correct images: src->base64, remove srcset, set "max-width: width" if available
$imgs = $out->getElementsByTagName('img');
foreach ($imgs as $img) {
    // $imgsrc = $img->attributes->getNamedItem('src')->value;
    // $imgtype = pathinfo($imgsrc, PATHINFO_EXTENSION);
    // if ($imgtype !== 'svg') {
    //     $imgdata = base64_encode(file_get_contents($imgsrc));
    //     $img->attributes->getNamedItem('src')->value = "data:image/$imgtype;base64,$imgdata";
    // }

    $img->removeAttribute('srcset');

    $imgwidth = $img->getAttribute('width');
    if ($imgwidth > 0) {
        $imgstyle = $img->getAttribute('style'); // returns an empty string if no attribute with the given name is found https://www.php.net/manual/en/domelement.getattribute.php
        $img->setAttribute('style', "max-width: {$imgwidth}px !important; $imgstyle");
    }
}

// Change links
$links = $xpath->query("//a[contains(@href, '$SOURCE_DOMAIN')][not(contains(concat(' ', normalize-space(@class), ' '), ' more-link '))]");
foreach ($links as $link) {
    $href = $link->getAttribute('href');
    if ((strpos($href, 'wp-content/uploads') !== false) || (strpos($href, "$SOURCE_DOMAIN/~") !== false)) continue;
    $newhref = str_replace($SOURCE_DOMAIN_EN, $DEST_DOMAIN_EN, $href, $occurred);
    if ($occurred <= 0) $newhref = str_replace($SOURCE_DOMAIN, $DEST_DOMAIN, $href);
    $link->setAttribute('href', $newhref);
}

// TODO: Change to CONTACT_FORM from TARGET
// // Correct form action url:
// $forms = $out->getElementsByTagName('form');
// foreach ($forms as $form) {
//     $form->attributes->getNamedItem('action')->value = "$parsedurl[scheme]://$parsedurl[host]" . $form->attributes->getNamedItem('action')->value;
// }

// Output prepared document:
$htmlOutput = $out->saveHTML();
if ($lastModifiedTime !== false) {
    file_put_contents($CACHE_FILE_PATH, $htmlOutput); // cache output
    touch($CACHE_FILE_PATH, $lastModifiedTime); // set mtime (because of server time difference)
}
echo $htmlOutput;

// } catch (Exception $e) {
//     echo 'Exception (' . $e->getCode() . '): ' .$e->getMessage();
// }

?>

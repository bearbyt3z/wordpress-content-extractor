# WordPress Content Extractor

The script extracts the content of a WordPress web page to present it dynamically on a different CMS/Website (originally on a different WordPress).

Whole categories can be extracted in a form of a single web page.

There are two different CSS files:
* post: styling for a single post;
* category: apply styles for category extraction.

<?php

$url = 'http://www.is.umk.pl/category/seminaria';

$h = get_headers($url, 1);
print_r($h);
$dt = NULL;
if (!$h || strpos($h[1], '200') !== false) {
    $dt = new \DateTime($h['Last-Modified']); // php 5.3
    echo $h['Last-Modified'];
}
$lmdate = date('Ymd', $h['Last-Modified']);

die('END');
$curl = curl_init($url);

// don't fetch the actual page, you only want headers
curl_setopt($curl, CURLOPT_NOBODY, true);

// stop it from outputting stuff to stdout
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

// attempt to retrieve the modification date
curl_setopt($curl, CURLOPT_FILETIME, true);

$result = curl_exec($curl);

if ($result === false) {
    die(curl_error($curl));
}

echo curl_getinfo($curl, CURLINFO_FILETIME);

$timestamp = curl_getinfo($curl, CURLINFO_FILETIME);
if ($timestamp != -1) { // otherwise unknown
    echo date("Y-m-d H:i:s", $timestamp); // etc
}

echo 'END';
?>
